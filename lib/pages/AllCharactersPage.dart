import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_battle/models/Character.dart';
import 'package:the_battle/models/Player.dart';
import 'package:the_battle/widgets/CharacterDetails.dart';
import 'package:the_battle/widgets/CharacterMaster.dart';
import 'package:the_battle/data/list-characters.dart' as listCharacters;
import 'package:the_battle/widgets/layout/TheBattleBottomNavigationBar.dart';
import 'package:the_battle/pages/TeamPage.dart';

class AllCharactersPage extends StatefulWidget{

  static const routeName = '/';
  static const routeIndex = 0;

  final Player player;

  const AllCharactersPage({ Key key, this.player }) : super(key: key);

  @override
  _AllCharactersPage createState() => _AllCharactersPage();

}

class _AllCharactersPage extends State<AllCharactersPage>{
  final List<Character> _characters = listCharacters.characters;
  Character _selectedCharacter;

  @override
  Widget build(BuildContext context) {

    void _onIndexChange(int index){
      if(index == TeamPage.routeIndex){
        Navigator.pushNamed(context, TeamPage.routeName).then((value){
          setState(() {});
        });
      }
    }

    void _onCharacterAdded(Character character){
      Navigator.pushNamed(
          context,
          TeamPage.routeName,
          arguments: character
      ).then((value){
        setState(() {
          _selectedCharacter = null;
        });
      });
    }

    void _onCharacterPreviewEvent(Character character){
      setState(() {
        _selectedCharacter = character;
      });
    }

    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Image(
              image: AssetImage('assets/images/the-battle-logo.png'),
              width: 100,
            )
          ],
        )),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if(_selectedCharacter != null)
              CharacterDetails(character: _selectedCharacter, parentCallback: _onCharacterAdded),
            Expanded(child: CharacterMaster(characters: _characters, parentCallback: _onCharacterPreviewEvent, player: this.widget.player))
          ],
        ),
      ),
      bottomNavigationBar: TheBattleBottomNavigationBar(selectedIndex : AllCharactersPage.routeIndex, onIndexChange: _onIndexChange),
    );
  }
}