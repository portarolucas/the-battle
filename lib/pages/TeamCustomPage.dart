import 'package:flutter/material.dart';
import 'package:the_battle/models/Player.dart';

class TeamCustomPage extends StatefulWidget {
  TeamCustomPage({Key key, this.player}) : super(key: key);

  final Player player;

  @override
  _TeamCustomPageState createState() => _TeamCustomPageState();
}

class _TeamCustomPageState extends State<TeamCustomPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Image(
                image: AssetImage('assets/images/the-battle-logo.png'),
                width: 100,
              )
            ],
          )),
      body: Center(
        child: Text(
          "Votre équipe a bien été validé ${this.widget.player.firstname}"
        )
      ),
    );
  }
}
