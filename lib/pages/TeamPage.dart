import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_battle/models/Character.dart';
import 'package:the_battle/models/Player.dart';
import 'package:the_battle/models/Team.dart';
import 'package:the_battle/pages/AllCharactersPage.dart';
import 'package:the_battle/utils/snackBarAlert.dart';
import 'package:the_battle/widgets/CharacterDetails.dart';
import 'package:the_battle/widgets/CharacterMaster.dart';
import 'package:the_battle/data/list-characters.dart' as listCharacters;
import 'package:the_battle/widgets/layout/TheBattleBottomNavigationBar.dart';

import 'TeamCustomPage.dart';

class TeamPage extends StatefulWidget{

  static const routeName = '/team';
  static const int routeIndex = 1;

  final Player player;

  const TeamPage({ Key key, this.player }) : super(key: key);

  @override
  _TeamPage createState() => _TeamPage();

}

class _TeamPage extends State<TeamPage>{

  Character CharacterAdded = null;

  @override
  Widget build(BuildContext context) {
    void _onIndexChange(int index){
      if(index == AllCharactersPage.routeIndex){
        Navigator.pop(context);
      }
    }

    try{
      Character character = ModalRoute.of(context).settings.arguments;
      if(character != null && CharacterAdded == character){
        character = null;
      }
      if(character != null){
        CharacterAdded = character;
        int returnedReponse = this.widget.player.team.addCharacter(character);
        switch(returnedReponse){
          case 1:
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(snackBarAlert("Le caractère a bien été ajouté à l'équipe.", "success"));
            break;
          case 2:
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(snackBarAlert("Le caractère ${character.name} existe déjà dans l'équipe", "error"));
            break;
          case 3:
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(snackBarAlert("Votre équipe est déjà complète, vous ne pouvez plus ajouter de caractère. (nombre maximum : ${Team.maxCharactersNumber.toString()})", "error"));
            break;
        }
      }

    }catch(e){
      //no parameters passed - on fait rien
    }

    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "My Team",
                style: TextStyle(color: Theme.of(context).primaryColor, fontFamily: 'Knewave', fontSize: 25),
              )
            ],
          )),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(Colors.green[400])
                          ),
                          child: Text(
                            "Valider l'équipe",
                            style: TextStyle(
                                fontFamily: 'Knewave', fontSize: 20, color: Colors.white
                            ),
                          ),
                          onPressed: (){
                            if(this.widget.player.team.validateTeam()){
                              ScaffoldMessenger.of(context).hideCurrentSnackBar();
                              Navigator.push(
                                context,
                                PageTransition(
                                  type: PageTransitionType.fade,
                                  child: TeamCustomPage(player: this.widget.player),
                                ),
                              );
                            }
                            else{
                              ScaffoldMessenger.of(context).hideCurrentSnackBar();
                              ScaffoldMessenger.of(context).showSnackBar(snackBarAlert("Impossible de valider l'équipe, vérifiez que vous avez bien ${Team.maxCharactersNumber} caractère(s) séléctionné(s)", "warning"));
                            }
                          },
                        ),
                        for(Character character in widget.player.team.characters)
                          InkWell(
                            child: Card(
                              color: (character.autoSelected) ? Colors.red[600] : Colors.red[400],
                              child: ListTile(
                                leading: Padding(
                                  padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                                  child: Image(
                                    image: AssetImage(character.imagePath()),
                                    width: 100,
                                    height: 50,
                                  ),
                                ),
                                title: Text(
                                  character.name,
                                  style: TextStyle(
                                      fontFamily: 'Knewave', fontSize: 20, color: Colors.white
                                  ),
                                ),
                                trailing: Padding(
                                  padding: EdgeInsets.only(right: 20),
                                  child: ElevatedButton(
                                    child: (character.autoSelected) ? Icon(Icons.lock, color: Colors.white) : Icon(Icons.remove_circle, color: Colors.white),
                                    onPressed: () {
                                      if(character.autoSelected == false){
                                        this.widget.player.team.deleteCharacter(character);
                                        setState(() {});
                                      }else{
                                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                                        ScaffoldMessenger.of(context).showSnackBar(snackBarAlert("Vous ne pouvez pas supprimer un caractère pré séléctionné", "error"));
                                      }
                                    },
                                  )
                                ),
                              ),
                            ),
                          )
                        ,
                      ],
                    ),
                  )
              )
            ],
          ),
        ),
      bottomNavigationBar: TheBattleBottomNavigationBar(selectedIndex : TeamPage.routeIndex, onIndexChange: _onIndexChange),
    );
  }
}