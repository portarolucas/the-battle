import 'package:flutter/material.dart';
import 'package:the_battle/pages/TeamCustomPage.dart';
import 'package:the_battle/pages/AllCharactersPage.dart';
import 'package:the_battle/models/Player.dart';
import 'package:the_battle/pages/TeamPage.dart';

class TheBattleApp extends StatefulWidget {

  @override
  _TheBattleAppState createState() => _TheBattleAppState();
}

class _TheBattleAppState extends State<TheBattleApp>{
  final Player _player = Player("uuid", "PORTARO", "Lucas", "portarolucas@doe.com", 1);

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'The Battle App',
      theme: ThemeData(
        fontFamily: 'Knewave-Regular',
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => AllCharactersPage(player: _player),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/team': (context) => TeamPage(player: _player),
      },
    );
  }
}
