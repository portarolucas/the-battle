import 'package:the_battle/models/Character.dart';
import 'package:the_battle/models/Player.dart';
import 'package:the_battle/data/list-characters.dart' as Data;
import 'dart:math';
import 'package:collection/collection.dart';

class Team{
  final String uuid;
  String name = "My Team";
  bool validated = false;

  List<Character> characters;
  static const int maxCharactersNumber = 5;

  Team(String this.uuid){
    characters = [];
    characters.addAll(this.randomChar());
  }

  List<Character> randomChar(){
    List<Character> charactersRandom;
    charactersRandom = [];
    int size = Data.characters.length - 1;
    if(size <= 0) {
      return null;
    }
    else{
      Random random = new Random();
      int randomNumber1 = random.nextInt(size);
      int randomNumber2 = random.nextInt(size);

      if(randomNumber1 == randomNumber2){
        randomNumber2++;
        if(randomNumber2 > size)
          randomNumber2 -= 2;
      }

      Character char1 = Data.characters[randomNumber1];
      Character char2 = Data.characters[randomNumber2];
      char1.autoSelected = true;
      char1.selected = true;
      char2.autoSelected = true;
      char2.selected = true;

      charactersRandom.add(char1);
      charactersRandom.add(char2);
      return charactersRandom;
    }
  }

  /**
   *
   * int addCharacter(Character)
   *
   * Summary of the addCharacter function:
   *
   * function used for adding Character to the actual team.
   *
   * Parameters   : Character
   *
   * Return Value :
   * - 1 : success the character was added to the team
   * - 2 : error the character was already selected in the team
   * - 3 : error the maximum character size was reached
   *
   * Description:
   *
   * The addCharacter function, used for adding Character to the actual team if :
   * - the caracter was not selected before by the user or the constructor (selected automaticly at the start)
   * - there is still place in the team
   *
   */
  int addCharacter(Character newCharacter){
    if(this.characters.length < Team.maxCharactersNumber){
      Character _character = characters.firstWhereOrNull((element) => element.uuid == newCharacter.uuid);
      if(_character != null && _character.selected == true)
        return 2;
      newCharacter.selected = true;
      this.characters.add(newCharacter);
      return 1;
    }else{
      return 3;
    }
  }

  void deleteCharacter(Character characterToDelete){
    this.characters.removeWhere((character) => characterToDelete.uuid == character.uuid && character.autoSelected != true);
  }

  bool validateTeam(){
    if(this.characters.length >= Team.maxCharactersNumber){
      validated = true;
      return true;
    }else{
      return false;
    }
  }
}