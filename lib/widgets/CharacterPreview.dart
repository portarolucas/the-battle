import 'package:flutter/material.dart';
import 'package:the_battle/models/Character.dart';

class CharacterPreview extends StatefulWidget{

  final Character character;
  final bool visited;
  final int isAlreadySelected;
  const CharacterPreview({Key key, this.character, this.parentCallback, this.visited, this.isAlreadySelected}) : super(key: key);

  final Function parentCallback;

  @override
  _CharacterPreviewState createState() => _CharacterPreviewState();
}

class _CharacterPreviewState extends State<CharacterPreview> {
  bool _selected = false;
  Color bgColor;

  //color: widget.visited ? Colors.red[300] : Colors.red[400]

  void _init(){
    switch(this.widget.isAlreadySelected){
      case 0:
        if(this.widget.visited)
          bgColor = Colors.red[300];
        else
          bgColor = Colors.red[500];
        break;
      case 1:
        if(this.widget.visited)
          bgColor = Colors.green[300];
        else
          bgColor = Colors.green[500];
        break;
      case 2:
        if(this.widget.visited)
          bgColor = Colors.orange[300];
        else
          bgColor = Colors.orange[500];
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    _init();

    return InkWell(
      onTap: () {
        this.widget.parentCallback(widget.character);
        setState(() {//sert plus à rien A SUPPRIMER
          _selected = !_selected;
        });
      },
      child: Card(
        color: bgColor,
        child: ListTile(
          leading: Padding(
            padding: EdgeInsets.all(10),
            child: Image(
              image: AssetImage(widget.character.imagePath()),
              width: 100,
            ),
          ),
          title: Text(
            widget.character.name,
            style: TextStyle(
                fontFamily: 'Knewave', fontSize: 20, color: Colors.white
            ),
          ),
        ),
      ),
    );
  }
}