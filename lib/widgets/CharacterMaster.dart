import 'package:flutter/material.dart';
import 'package:the_battle/models/Character.dart';
import 'package:the_battle/models/Player.dart';
import 'package:the_battle/widgets/CharacterPreview.dart';
import 'package:the_battle/widgets/CharacterDetails.dart';
import 'package:collection/collection.dart';

class CharacterMaster extends StatefulWidget {

  final List <Character> characters;
  final Player player;

  const CharacterMaster(
      {Key key, this.characters, this.parentCallback, this.player})
      : super(key: key);

  final Function parentCallback;

  @override
  _CharacterMasterState createState() => _CharacterMasterState();
}
class _CharacterMasterState extends State<CharacterMaster>{

  Character _selectedCharacter = null;

  void _onCharacterPreviewEvent(Character character){
    setState(() {
      this._selectedCharacter = character;
    });
    widget.parentCallback(character);
  }

  /**
   *
   * bool isVisited(Character)
   *
   * Summary of the isVisited function:
   *
   * function used for know if a character is actually selected by the user (in the UX).
   *
   * Parameters   : Character
   *
   * Return Value :
   * - false : not selected
   * - true : selected
   */
  bool isVisited(Character character){
    if(this._selectedCharacter == null){
      return false;
    }else if(this._selectedCharacter.uuid == character.uuid){
      return true;
    }else{
      return false;
    }
  }

  /**
   *
   * int isAlreadySelected(Character)
   *
   * Summary of the isAlreadySelected function:
   *
   * function used for know if a character is already selected in the Player team and if he is autoselected by the app.
   *
   * Parameters   : Character
   *
   * Return Value :
   * - 0 : not selected
   * - 1 : selected by the user
   * - 2 : auto selected by the app
   */
  int isAlreadySelected(Character character){
    Character _character = this.widget.player.team.characters.firstWhereOrNull((element) => element.uuid == character.uuid);
    if(_character != null && _character.autoSelected == true){
      return 2;
    }else if(_character != null)
      return 1;
    else
      return 0;
  }

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          for(Character character in widget.characters ) CharacterPreview(character: character, parentCallback: _onCharacterPreviewEvent,
            visited: isVisited(character), isAlreadySelected: isAlreadySelected(character)),
        ],
      ),
    );
  }
}